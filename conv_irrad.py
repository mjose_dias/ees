import pandas as pd
import numpy as np

def csv_to_npy(input_csv, output_npy):
    # Lê o arquivo CSV usando pandas
    df = pd.read_csv(input_csv)
    
    # Salva os dados em um arquivo .npy
    np.save(output_npy, df.values)
    print("Dados do arquivo CSV salvos em", output_npy)

# Substitua 'input.csv' pelo caminho do seu arquivo CSV e 'output.npy' pelo nome do arquivo .npy de saída
#csv_to_npy('sluis2016/SLZ1612ED/SLZ1612ED.csv', 'arquivos_npy/mes_12.npy')
csv_to_npy(arquivo.csv,arquivo.npy)

